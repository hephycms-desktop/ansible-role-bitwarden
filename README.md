Role Name
=========

Install Bitwarden Dektop app and Bitwarden CLI

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.bitwarden

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
